"""
Project Euler Problem #5
=========================

2520 is the smallest number that can be divided by each of the numbers
from 1 to 10 without any remainder.

What is the smallest number that is evenly divisible by all of the numbers
from 1 to 20?
"""
step = 19*17*13*11*7*5*3*2
value = step
while sum([value%k for k in range(11,21)]):
	#if not value%10000: print value
	value = value+step
print value
"""
It must be divisible by the primes!!
"""

