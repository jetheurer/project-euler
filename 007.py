"""
Project Euler Problem #7
=========================

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
that the 6^th prime is 13.

What is the 10001^st prime number?
"""


def isprime(n, check):
	for i in check:
		if not n%i:
			return False
	else: return True

primes = []
i=2
while len(primes)<10001:
	if isprime(i,primes):
		primes.append(i)
	i = i+1

print primes[-1]
