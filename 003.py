"""
Project Euler Problem #3
=========================

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143?
"""

def isprime(value):
	for i in range(int(value))[2:-1]:
		if not value%i:
			return False
			break
	return True

N = 600851475143
i = 2.0
keep =0

while i<N/i:
	if not N%i:
		if isprime(i):			
			keep = i
	i = i+1.0

print int(keep)
