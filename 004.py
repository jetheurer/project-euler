"""
Project Euler Problem #4
=========================

A palindromic number reads the same both ways. The largest palindrome made
from the product of two 2-digit numbers is 9009 = 91 * 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""
answer = []
for i in range(100,999)[::-1]:
	for j in range(100,i)[::-1]:
			value = i*j
			reverse = str(value%1000)[::-1]
			if int(reverse)==value/1000:
				answer.append(value)
print max(answer)
