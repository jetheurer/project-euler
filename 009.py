"""
Project Euler Problem #9
=========================

A Pythagorean triplet is a set of three natural numbers, a < b < c, for
which,
                             a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""
import sys
max_value = 998

keep = 0
while not keep:
	for a in range(max_value):
		for b in range(max_value):
			c = 1000-b-a
			if a**2 + b**2 == c**2 and  a and  b and  c:
				keep= a*b*c
print keep
