virtualenv venv --no-site-packages
source venv/bin/activate

pip install -r requirements.txt

deactivate

bash check.sh > status.log